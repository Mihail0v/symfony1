## Установка

1. Настройка docker
```
cp ./.docker/.env.nginx ./.docker/.env.nginx.local
```
В скопированном файле задать переменную `NGINX_BACKEND_DOMAIN`

После этого выполнить
```
cd ./.docker
docker-compose up -d
docker-compose run php composer install --no-interaction
docker-compose run php bin/console doctrine:migrations:migrate --no-interaction
```

3. Поднять frontend
```
cd ./frontend
npm install
npm run build   
```
4. Запуск тестов
`docker-compose run php bin/phpunit`
