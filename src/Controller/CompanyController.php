<?php

namespace App\Controller;

use App\Entity\Company;
use App\Service\CompanyService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CompanyController extends AbstractController
{
    private CompanyService $companyService;
    private ValidatorInterface $validator;

    public function __construct(CompanyService $companyService, ValidatorInterface $validator)
    {
        $this->companyService = $companyService;
        $this->validator = $validator;
    }

    #[Route('/api/companies', methods: ['GET'])]
    public function index(): JsonResponse
    {
        $companies = $this->companyService->getAllCompanies();
        return $this->jsonWithData($companies);
    }

    #[Route('/api/companies', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $company = new Company();
        $company->setInn($data['inn']);
        $company->setName($data['name']);
        $company->setAddress($data['address']);

        $errors = $this->validator->validate($company);
        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[$error->getPropertyPath()][] = $error->getMessage();
            }
            return $this->jsonWithErrors($errorMessages);
        }

        $this->companyService->createCompany($company);

        return $this->jsonWithData($company);
    }

    #[Route('/api/companies/{id}', methods: ['PUT'])]
    public function update(int $id, Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $company = $this->companyService->getCompanyById($id);
        if (!$company) {
            return $this->jsonWithData(['message' => 'Company not found'], Response::HTTP_NOT_FOUND);
        }

        $company->setInn($data['inn']);
        $company->setName($data['name']);
        $company->setAddress($data['address']);

        $errors = $this->validator->validate($company);
        if (count($errors) > 0) {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[$error->getPropertyPath()][] = $error->getMessage();
            }
            return $this->jsonWithErrors($errorMessages);
        }

        $this->companyService->updateCompany($company);

        return $this->jsonWithData($company);
    }

    #[Route('/api/companies/{id}', methods: ['DELETE'])]
    public function delete(int $id): JsonResponse
    {
        $company = $this->companyService->getCompanyById($id);
        if (!$company) {
            return $this->json(['message' => 'Company not found'], Response::HTTP_NOT_FOUND);
        }

        $this->companyService->deleteCompany($company);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/api/companies/{id}', methods: ['GET'])]
    public function show(int $id): JsonResponse
    {
        $company = $this->companyService->getCompanyById($id);
        if (!$company) {
            return $this->json(['message' => 'Company not found'], Response::HTTP_NOT_FOUND);
        }

        return $this->jsonWithData($company);
    }

    private function jsonWithData($data, int $statusCode = Response::HTTP_OK): JsonResponse
    {
        return $this->json(['data' => $data], $statusCode);
    }

    private function jsonWithErrors(array $errors): JsonResponse
    {
        return $this->json(['errors' => $errors], Response::HTTP_BAD_REQUEST);
    }
}
