<?php

namespace App\Entity;

use App\Repository\CompanyRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CompanyRepository::class)]
class Company
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['company'])]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 12, unique: true)]
    #[Assert\NotBlank]
    #[Assert\Length(
        min: 12,
        max: 12,
        exactMessage: "ИНН должен содержать ровно {{ limit }} символов."
    )]
    #[Groups(['company'])]
    private string $inn;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Length(
        max: 255,
        maxMessage: "Название не должно превышать {{ limit }} символов."
    )]
    #[Groups(['company'])]
    private string $name;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Length(
        max: 255,
        maxMessage: "Юридический адрес не должен превышать {{ limit }} символов."
    )]
    #[Groups(['company'])]
    private string $address;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInn(): string
    {
        return $this->inn;
    }

    public function setInn(string $inn): void
    {
        $this->inn = $inn;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): void
    {
        $this->address = $address;
    }
}
