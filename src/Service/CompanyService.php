<?php

namespace App\Service;

use App\Entity\Company;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class CompanyService
{
    private EntityManagerInterface $entityManager;
    private CacheInterface $cache;

    public function __construct(EntityManagerInterface $entityManager, CacheInterface $cache)
    {
        $this->entityManager = $entityManager;
        $this->cache = $cache;
    }

    public function getAllCompanies(): array
    {
        return $this->cache->get('companies_list', function (ItemInterface $item) {
            $item->expiresAfter(3600);
            return $this->entityManager->getRepository(Company::class)->findBy([], ['id' => 'DESC']);
        });
    }

    public function getCompanyById(int $id): ?Company
    {
        return $this->cache->get("company_$id", function (ItemInterface $item) use ($id) {
            $item->expiresAfter(3600);
            return $this->entityManager->getRepository(Company::class)->find($id);
        });
    }

    public function createCompany(Company $company): Company
    {
        $this->entityManager->persist($company);
        $this->entityManager->flush();

        $this->cache->delete('companies_list');

        return $company;
    }

    public function updateCompany(Company $company): Company
    {
        $this->entityManager->flush();

        $this->cache->delete('companies_list');
        $this->cache->delete("company_{$company->getId()}");

        return $company;
    }

    public function deleteCompany(Company $company): void
    {
        $this->entityManager->remove($company);
        $this->entityManager->flush();

        $this->cache->delete('companies_list');
        $this->cache->delete("company_{$company->getId()}");
    }
}
