<?php

namespace App\Tests\e2e\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CompanyControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
        $client->request('GET', '/api/companies');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreate(): void
    {
        $companyData = [
            'inn' => '123456789012',
            'name' => 'Test Company',
            'address' => 'Test Address',
        ];
        $client = static::createClient();
        $client->request('POST', '/api/companies', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode($companyData));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        ['data' => $response] = json_decode($client->getResponse()->getContent(), true);
        unset($response['id']);

        $this->assertEquals($companyData, $response);
    }

    /**
     * @dataProvider invalidCompanyDataProvider
     */
    public function testCreateValidation(array $invalidData, array $expectedErrors): void
    {
        $client = static::createClient();
        $client->request('POST', '/api/companies', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode($invalidData));

        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        ['errors' => $response] = json_decode($client->getResponse()->getContent(), true);
        foreach ($expectedErrors as $field => $error) {
            $this->assertContains($error, $response[$field]);
        }
    }

    public function invalidCompanyDataProvider(): array
    {
        return [
            'empty data' => [
                [
                    'inn' => '',
                    'name' => '',
                    'address' => '',
                ],
                [
                    'inn' => 'This value should not be blank.',
                    'name' => 'This value should not be blank.',
                    'address' => 'This value should not be blank.',
                ],
            ],
            'short INN' => [
                [
                    'inn' => '123',
                    'name' => 'Valid Name',
                    'address' => 'Valid Address',
                ],
                [
                    'inn' => 'ИНН должен содержать ровно 12 символов.',
                ],
            ],
            'long INN' => [
                [
                    'inn' => '1234567890123',
                    'name' => 'Valid Name',
                    'address' => 'Valid Address',
                ],
                [
                    'inn' => 'ИНН должен содержать ровно 12 символов.',
                ],
            ],
        ];
    }

    public function testUpdate(): void
    {
        $client = static::createClient();

        $companyData = [
            'inn' => '123456789012',
            'name' => 'Test Company',
            'address' => 'Test Address',
        ];
        $client->request('POST', '/api/companies', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode($companyData));
        ['data' => $response] = json_decode($client->getResponse()->getContent(), true);
        $companyId = $response['id'];

        $updatedData = [
            'inn' => '123456789012',
            'name' => 'Updated Company',
            'address' => 'Updated Address',
        ];
        $client->request('PUT', '/api/companies/' . $companyId, [], [], ['CONTENT_TYPE' => 'application/json'], json_encode($updatedData));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        ['data' => $response] = json_decode($client->getResponse()->getContent(), true);
        unset($response['id']);

        $this->assertEquals($updatedData, $response);
    }

    public function testDelete(): void
    {
        $client = static::createClient();

        $companyData = [
            'inn' => '123456789012',
            'name' => 'Test Company',
            'address' => 'Test Address',
        ];
        $client->request('POST', '/api/companies', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode($companyData));
        ['data' => $response] = json_decode($client->getResponse()->getContent(), true);
        $companyId = $response['id'];

        $client->request('DELETE', '/api/companies/' . $companyId);

        $this->assertEquals(204, $client->getResponse()->getStatusCode());

        $client->request('GET', '/api/companies/' . $companyId);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    public function testShow(): void
    {
        $client = static::createClient();

        $companyData = [
            'inn' => '123456789012',
            'name' => 'Test Company',
            'address' => 'Test Address',
        ];
        $client->request('POST', '/api/companies', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode($companyData));
        ['data' => $response] = json_decode($client->getResponse()->getContent(), true);
        $companyId = $response['id'];

        $client->request('GET', '/api/companies/' . $companyId);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        ['data' => $response] = json_decode($client->getResponse()->getContent(), true);
        unset($response['id']);

        $this->assertEquals($companyData, $response);
    }
}
