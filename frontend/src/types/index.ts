export interface Company {
  id: number | null;
  name: string;
  address: string;
  inn: string;
}

export interface ApiResponse<T> {
  data: T;
}
