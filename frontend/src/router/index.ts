import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import CompanyList from '@/components/CompanyList.vue';
import CompanyForm from '@/components/CompanyForm.vue';
import CompanyDetails from '@/components/CompanyDetails.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/companies',
  },
  {
    path: '/companies',
    name: 'CompanyList',
    component: CompanyList,
  },
  {
    path: '/companies/new',
    name: 'CreateCompany',
    component: CompanyForm,
  },
  {
    path: '/companies/edit/:id',
    name: 'EditCompany',
    component: CompanyForm,
    props: true,
  },
  {
    path: '/companies/:id',
    name: 'CompanyDetails',
    component: CompanyDetails,
    props: true,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
