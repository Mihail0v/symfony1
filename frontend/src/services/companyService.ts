import axios, { AxiosResponse } from 'axios';
import { Company, ApiResponse } from '@/types';

const API_URL = 'http://localhost/api/companies';

export default {
  async getAll(): Promise<ApiResponse<Company[]>> {
    try {
      const response: AxiosResponse<ApiResponse<Company[]>> = await axios.get(API_URL);
      return response.data;
    } catch (error) {
      throw new Error(`Error fetching companies: ${error}`);
    }
  },

  async get(id: number): Promise<ApiResponse<Company>> {
    try {
      const response: AxiosResponse<ApiResponse<Company>> = await axios.get(`${API_URL}/${id}`);
      return response.data;
    } catch (error) {
      throw new Error(`Error fetching company with ID ${id}: ${error}`);
    }
  },

  async create(data: Company): Promise<ApiResponse<Company>> {
    try {
      const response: AxiosResponse<ApiResponse<Company>> = await axios.post(API_URL, data);
      return response.data;
    } catch (error) {
      throw new Error(`Error creating company: ${error}`);
    }
  },

  async update(id: number, data: Company): Promise<void> {
    try {
      await axios.put(`${API_URL}/${id}`, data);
    } catch (error) {
      throw new Error(`Error updating company with ID ${id}: ${error}`);
    }
  },

  async delete(id: number): Promise<void> {
    try {
      await axios.delete(`${API_URL}/${id}`);
    } catch (error) {
      throw new Error(`Error deleting company with ID ${id}: ${error}`);
    }
  },
};
